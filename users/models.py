from email.headerregistry import Address
from socket import AF_AX25
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField()  # Biography of user

    def __str__(self):
        return self.user.username

    @receiver(post_save, sender=User)
    def create_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def update_profile(sender, instance, **kwargs):
        instance.profile.save()

class UploadFile(models.Model):
    uploadedFile = models.FileField(upload_to='uploads/csv/')
    title=models.CharField(max_length=150)
    date_time_of_upload=models.DateTimeField(auto_now=True)

class Read_excel(models.Model):
    user_id = models.IntegerField()
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone_number = models.IntegerField()
    address = models.CharField(max_length=100)
    

