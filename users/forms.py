from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django import forms
from django.contrib.auth.models import User


class LoginForm(AuthenticationForm):
    pass


class SigninForm(UserCreationForm):
    # username and password are charged for use UserCreationForm
    email = forms.EmailField(label='email')
    first_name = forms.CharField(label='first name')
    last_name = forms.CharField(label='last name')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

    
