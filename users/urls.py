from django.urls import path
from .views import LoginView, SigninView, LogoutView, uploadFileExcel, home_view

app_name = 'users'

urlpatterns = [
    path('', home_view, name='home'),
    path("login/", LoginView.as_view(), name='login'),
    path("signin/", SigninView.as_view(), name='signin'),
    path("logout/", LogoutView.as_view(), name='logout'),
    path("upload/", uploadFileExcel, name='upload'),
]
