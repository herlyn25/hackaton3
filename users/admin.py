from django.contrib import admin


# Register your models here.
from users.models import Profile, Read_excel

admin.site.register(Profile)
admin.site.register(Read_excel)