from ctypes import addressof
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, RedirectView, FormView, TemplateView, ListView
from .models import Profile, Read_excel
import pandas as pd

import pathlib
from users.forms import LoginForm, SigninForm
from users.models import Profile

from users import models


@login_required(login_url='users:login')
def home_view(request):
    return render(request, 'base.html')


class LoginView(FormView):
    form_class = LoginForm
    template_name = 'users/login.html'
    success_url = reverse_lazy('users:home')

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)

        if user is not None and user.is_active:
            login(self.request, user)
            return super(LoginView, self).form_valid(form)
        else:
            return self.form_invalid(form)


class LogoutView(RedirectView):
    url = reverse_lazy('users:login')

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, *kwargs)


class SigninView(CreateView):
    form_class = SigninForm
    model = User
    template_name = 'users/signin.html'
    success_url = reverse_lazy('users:login')

def upload_file(request):
    msg = ''
    documents = models.UploadFile.objects.all()
    if request.method == 'POST':
        ext = ['.csv', '.xls', '.xlsx']
        file_title = request.POST["fileTitle"]
        upload_files = request.FILES['uploadedFile']
        # text.csv el csv es el sufijo del archivo
        extension = pathlib.Path(upload_files.name).suffix

        if extension in ext:
            document = models.UploadFile(
                title=file_title,
                uploadedFile=upload_files,
            )

            document.save()
            documents = models.UploadFile.objects.all()  # SELECT * FROM documents
        else:
            document = None
            msg = 'Solo se permiten archivos con extensiones .csv, .xls, .xlsx'
    return render(request, "upload_file.html", context={'files': documents, 'msg': msg})

def uploadFileExcel(request):
    data_excel = None
    msg = ''
    documents = None
    context = None
    if request.method == "POST":
        ext = ['.xls', '.xlsx']
        file_title = request.POST["fileTitle"]
        upload_files = request.FILES['uploadedFile']

        extension = pathlib.Path(upload_files.name).suffix

        if extension in ext:
            document = models.UploadFile(
                title=file_title,
                uploadedFile=upload_files
            )
            document.save()

            data = read_xlsx(document)
            imported_data= pd.DataFrame(data)
            count_data=len(imported_data)  # count rows

            count_userid = imported_data['user_id']  # get dataframe values
            count_userid = count_userid.value_counts()  # get counted values
            count_userid = count_userid.to_frame().to_html


            count_name = imported_data['first_name']  # get dataframe values
            count_name = count_name.value_counts()  # get counted values
            count_name = count_name.to_frame().to_html  # get key list
           
            #  hacer igual que en las lineas 159-161

            count_lastname = imported_data['last_name']  # get dataframe values
            count_lastname = count_lastname.value_counts()
            count_lastname = count_lastname.to_frame().to_html  # get key list
            
            #  hacer igual que en las lineas 159-161

            count_phonenumber = imported_data['phone_number']  # get dataframe values
            count_phonenumber = count_phonenumber.value_counts()
            count_phonenumber = count_phonenumber.to_frame().to_html  # g

            count_address = imported_data['address']  # get dataframe values
            count_address = count_address.value_counts()
            count_address = count_address.to_frame().to_html  # g

            print(count_name)
            context = {'files': data.values.tolist(),
                       'count_data': count_data,
                       'count_userid':count_userid,
                       'count_name': count_name,
                       'count_lastname': count_lastname,
                       'count_phonenumber': count_phonenumber,
                       'count_address': count_address,
                       'msg': msg,
                       }
        else:
            msg = 'Archivo No Valido'
    return render(request, "users/upload_excel.html", context=context)

def read_xlsx(document):
    content = None
    if document:
        content = pd.read_excel(
            document.uploadedFile.path)  # lee el archivo y lo convierte en una lista de diccionarios
        columns = ['user_id', 'first_name', 'last_name', 'phone_number', 'address']
        for data in columns:  # add the column to the object
            if data == "Order Date" or data == "Ship Date":  # reformatea las filas de fecha
                date = pd.to_datetime(content[data]).apply(lambda x: x.date())  # columna reformada
                content[data] = date
        for index, row in content.iterrows():
            Read_excel.objects.create(
                user_id=row['user_id'],
                first_name=row['first_name'],
                last_name=row['last_name'],
                phone_number=row['phone_number'],
                address=row['address'],
                
            ).save()

    return content