from django.contrib.auth.models import AbstractUser, User
from django.db import models


# Create your models here.

class Room(models.Model):
    number_room = models.IntegerField()
    state = models.BooleanField(default=False)


class Booking(models.Model):
    date = models.DateField(auto_now=True)
    room = models.ForeignKey(Room, on_delete=models.DO_NOTHING)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date_booking = models.DateField()
    state = models.BooleanField()
