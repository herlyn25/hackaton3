from Email.forms import ReviewForm
from django.views.generic.edit import FormView
from django.http import HttpResponse


class ReviewEmailView(FormView):
    template_name = 'email/review.html'
    form_class = ReviewForm

    def form_valid(self, form):
        form.send_email()
        msg = "Gracias Por comunicarse con nostros"
        return HttpResponse(msg)
